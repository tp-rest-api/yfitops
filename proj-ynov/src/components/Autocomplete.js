import React, { useContext, useEffect, useState } from "react";
import axios from "axios";
import TextField from "@material-ui/core/TextField";
import { useHistory } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import AutocompleteVue from "./AutocompleteVue";
import Autocomplete from "@material-ui/lab/Autocomplete";
import CircularProgress from "@material-ui/core/CircularProgress";
import UserContext from "../contexte/user/UserContext";

const useStyles = makeStyles((theme) => ({
  userInput: {
    float: "left",
    marginLeft: "22px",
    borderRadius: 25,
    backgroundColor: "white",
    width: 500,
    textDecoration: "none",
    fontFamily: "'oboto, sans-serif",
  },
}));

export default function AutocompleteInput() {
  const classes = useStyles();
  let history = useHistory();
  const userContext = useContext(UserContext);
  const [options, setOptions] = useState([]);

  useEffect(() => {
    if (options.length !== 0) {
      console.log(options);
      history.push({ pathname: "/search", state: { detail: options } });
    }
  }, [options]);

  const test = (e) => {
    e.preventDefault();
    const val = e.target.value;
    if (val.length > 1) {
      axios
        .get(`http://localhost:8080/titre/search/${e.target.value}`, {
          headers: { Authorization: `Bearer ${userContext.user.accessToken}` },
        })
        .then((res) => {
          setOptions(res.data);
        });
    }
  };

  return (
    <TextField
      id="standard-basic"
      onChange={test}
      placeholder="Recherchez des musiques ..."
      className={classes.userInput}
    />
  );
}
