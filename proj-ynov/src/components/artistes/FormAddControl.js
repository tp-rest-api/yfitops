import React, { useContext } from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import axios from "axios";
import UserContext from "../../contexte/user/UserContext";

export default function FormAddControl() {
  const userContext = useContext(UserContext);

  const add = (event) => {
    const nom = event.target.name.value;
    const body = {
      nom: nom,
    };
    axios
      .post("http://localhost:8080/artiste", body, {
        headers: { Authorization: `Bearer ${userContext.user.accessToken}` },
      })
      .then((res) => {
        console.log(`${res} added`);
      });
  };

  return (
    <div>
      <form onSubmit={add} autoComplete="off">
        <TextField id="name" label="Name" />
        <Button type={"submit"} variant="contained" color="primary">
          Confirmer
        </Button>
      </form>
    </div>
  );
}
