import React, { useEffect, useState, useContext } from "react";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import ArtisteCard from "./ArtisteCard";
import Button from "@material-ui/core/Button";
import axios from "axios";
import ModalAdd from "./ModalAdd";
import UserContext from "../../contexte/user/UserContext";
import ModalAddTitre from "./ModalAddTitre";

export default function AutorsCarousel() {
  const [autors, setAutors] = useState([]);
  const userContext = useContext(UserContext);

  useEffect(() => {
    axios
      .get(`http://localhost:8080/artiste`, {
        headers: {
          Authorization: `Bearer ${userContext.user.accessToken}`,
        },
      })
      .then((res) => {
        setAutors(res.data);
      });
  }, []);

  const responsive = {
    superLargeDesktop: {
      // the naming can be any, depends on you.
      breakpoint: { max: 4000, min: 3000 },
      items: 10,
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 6,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 4,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 2,
    },
  };
  const placementStyle = {
    marginTop: "5%",
  };

  return (
    <div style={placementStyle}>
      <ModalAddTitre />
      <h2
        style={{
          marginBottom: "2%",
          marginLeft: "3%",
          fontFamily: "open-sans",
        }}
      >
        Artistes <ModalAdd></ModalAdd>
      </h2>
      <Carousel
        swipeable={true}
        draggable={false}
        responsive={responsive}
        ssr={true} // means to render carousel on server-side.
        infinite={true}
        autoPlay={true}
        autoPlaySpeed={5000}
        keyBoardControl={true}
        customTransition="transform 500ms ease-in-out"
        centerMode={true}
        transitionDuration={500}
        containerClass="carousel-container"
        removeArrowOnDeviceType={["tablet", "mobile"]}
        itemClass="carousel-item-padding-70-px"
      >
        {autors.map((autor, index) => (
          <ArtisteCard key={index} autor={autor}></ArtisteCard>
        ))}
      </Carousel>
    </div>
  );
}
