import React, { Component } from "react";
import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import { Link } from "react-router-dom";
import Typography from "@material-ui/core/Typography";

class ArtisteCard extends Component {
  getStyle = () => {
    return {
      borderRadius: "50%",
      margin: "28px",
    };
  };
  render() {
    const fontStyle = {
      textAlign: "center",
      display: "inline-block",
      textDecoration: "none",
      color: "white",
      size: "0.9 rem",
      fontFamily: "open-sans",
      boxShadow: "none",
    };
    const { id, name, image } = this.props.autor;
    console.log(this.props.autor);
    return (
      <div>
        <Link style={{ textDecoration: "none" }} to={`/autor/${id}`}>
          <Card
            key={id}
            style={{
              border: "2px solid black",
              margin: 0,
              maxWidth: 100,
              borderRadius: "70%",
            }}
          >
            <CardMedia
              style={{ width: 100, height: 100 }}
              image={image}
              title={name}
            />
          </Card>
        </Link>
        <Typography variant="body2" style={fontStyle}>
          {name}
        </Typography>
      </div>
    );
  }
}

// Artiste.propTypes = {
//   id: PropTypes..isRequired,
//   name: PropTypes.string.isRequired,
//   image: PropTypes.string.isRequired,
// };

export default ArtisteCard;
