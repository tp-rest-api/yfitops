import React, { useContext, useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import TabsInfo from "./tab";
import Button from "@material-ui/core/Button";
import PlayCircleOutlineIcon from "@material-ui/icons/PlayCircleOutline";
import { Typography } from "@material-ui/core";
import UserContext from "../../contexte/user/UserContext";
import axios from "axios";

export default function AutorDetails(detail) {
  const { id } = detail.match.params;
  const userContext = useContext(UserContext);
  const [autor, setAutor] = useState([]);

  useEffect(() => {
    axios
      .get(`http://localhost:8080/artiste/${id}`, {
        headers: { Authorization: `Bearer ${userContext.user.accessToken}` },
      })
      .then((res) => {
        setAutor(res.data);
      });
  }, []);
  const useStyles = makeStyles({
    root: {
      color: "white",
      fontWeight: "bold",
      top: "30%",
      left: "50%",
      transform: "translate(-50%, -50%)",
      zIndex: 2,
      textAlign: "center",
      position: "absolute",
      border: "2px solid black",
      maxWidth: 150,
      boxShadow: "0 0 2em black;",
      borderRadius: "70%",
    },
    button: {
      margin: 0,
    },
    titre: {
      fontWeight: "bold",
      top: "47%",
      left: "50%",
      transform: "translate(-50%, -50%)",
      zIndex: 2,
      textAlign: "center",
      textShadow: "1px 1px 2px black",
      position: "absolute",
    },
    center: {
      color: "white",
      fontWeight: "bold",
      top: "62%",
      left: "50%",
      transform: "translate(-50%, -50%)",
      zIndex: 2,
      textAlign: "center",
      position: "absolute",
    },
    backStyle: {
      height: 400,
      opacity: 0.8,
      width: "100%",
      backgroundImage: `url(${autor.image})`,
      backgrounDposition: "center",
      backgroundRepeat: "repeat",
      backgroundSize: "10em",
      filter: `blur(7px)`,
    },
    media: {
      width: 150,
      height: 150,
    },
  });
  const classes = useStyles();

  return (
    <div>
      <div className={classes.backStyle}></div>
      <Card className={classes.root}>
        <CardMedia
          className={classes.media}
          image={autor.image}
          title={id}
        ></CardMedia>
      </Card>
      <div className={classes.center}>
        <Button
          variant="contained"
          color="primary"
          className={classes.button}
          endIcon={<PlayCircleOutlineIcon />}
        >
          Lancer une chanson aléatoire
        </Button>
      </div>
      <div className={classes.titre}>
        <Typography variant="h3" style={{ color: "#FFFFFF" }}>
          {autor.name}
        </Typography>
      </div>
      <TabsInfo id={id}></TabsInfo>
    </div>
  );
}
