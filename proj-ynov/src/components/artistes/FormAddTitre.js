import React, { useContext, useState, useEffect } from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import axios from "axios";
import UserContext from "../../contexte/user/UserContext";
import Autocomplete from "@material-ui/lab/Autocomplete";

export default function FormAddTitre() {
  const userContext = useContext(UserContext);
  const [autors, setAutors] = useState([]);

  useEffect(() => {
    axios
      .get("http://localhost:8080/artiste", {
        headers: { Authorization: `Bearer ${userContext.user.accessToken}` },
      })
      .then((res) => {
        setAutors(res.data);
      });
  }, []);

  const add = (event) => {
    const nom = event.target.name.value;
    const duree = event.target.duree.value;
    const name = event.target.test.value;
    axios
      .get(`http://localhost:8080/artiste/search/${name}`, {
        headers: { Authorization: `Bearer ${userContext.user.accessToken}` },
      })
      .then((res) => {
        const body = {
          nom: nom,
          duree: duree,
          artiste: {
            id: res.data.id,
          },
        };
        axios
          .post("http://localhost:8080/titre", body, {
            headers: {
              Authorization: `Bearer ${userContext.user.accessToken}`,
            },
          })
          .then((res) => {
            console.log(`${res} added`);
          });
      });
  };

  return (
    <div>
      <form onSubmit={add} autoComplete="off">
        <TextField id="name" label="Name" />
        <TextField id="duree" label="Durée (exemple 00:02:45" />
        <Autocomplete
          freeSolo
          id="test"
          getOptionLabel={(option) => option.name}
          options={autors}
          renderInput={(params) => (
            <TextField
              id="autor"
              {...params}
              label="Auteur ..."
              margin="normal"
              variant="outlined"
              InputProps={{ ...params.InputProps, type: "search" }}
            />
          )}
        />
        <Button type={"submit"} variant="contained" color="primary">
          Confirmer
        </Button>
      </form>
    </div>
  );
}
