import React, { useContext, useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import axios from "axios";
import Paper from "@material-ui/core/Paper";
import UserContext from "../../contexte/user/UserContext";

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

function createData(id, nom, duree) {
  return { id, nom, duree };
}

export default function BasicTable(props) {
  const classes = useStyles();
  const [titles, setTitles] = useState([]);
  const userContext = useContext(UserContext);

  useEffect(() => {
    axios
      .get(`http://localhost:8080/artiste/${props.id}/titres`, {
        headers: { Authorization: `Bearer ${userContext.user.accessToken}` },
      })
      .then((res) => {
        setTitles(res.data);
      });
    const row = titles.map((title) => {
      createData(title.id, title.nom, title.duree);
    });
  }, []);

  return (
    <TableContainer
      style={{ backgroundColor: "inherit", marginBottom: "10%" }}
      component="div"
    >
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell style={{ color: "lightgrey" }}>#</TableCell>
            <TableCell style={{ color: "lightgrey" }}>TITRE</TableCell>
            <TableCell style={{ color: "lightgrey" }}>DUREE</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {titles.map((row) => (
            <TableRow hover key={row.id}>
              <TableCell style={{ color: "white" }}>{row.id}</TableCell>
              <TableCell style={{ color: "white" }}>{row.nom}</TableCell>
              <TableCell style={{ color: "white" }}>{row.duree}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
