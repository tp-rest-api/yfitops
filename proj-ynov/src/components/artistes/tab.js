import React, { useContext, useEffect, useState } from "react";
import PropTypes from "prop-types";
import SwipeableViews from "react-swipeable-views";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import BasicTable from "./TableMusic";
import axios from "axios";
import UserContext from "../../contexte/user/UserContext";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    "aria-controls": `full-width-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: "inherit",
    color: "white",
  },
  card: {
    maxWidth: 400,
    marginLeft: "3%",
    marginRight: "3%",
    marginBottom: "2%",
  },
  fontStyle: {
    display: "inline-block",
    color: "white",
    marginLeft: "4%",
    fontFamily: "Open-sans",
  },
  media: {
    height: 200,
    paddingTop: "56.25%", // 16:9
    border: "1px solid black",
  },
  tab: {
    color: "white",
  },
  container1: {
    width: "100%",
    height: "100%",
    overflow: "hidden",
    padding: "1% 0 0 1%",
  },
  container2: {
    marginLeft: "5%",
    height: "100%",
    width: 300,
    display: "inline-block",
    float: "left",
    margin: "0 1% 1% 0",
  },
}));

export default function TabsInfo(props) {
  const classes = useStyles();
  const theme = useTheme();
  const [value, setValue] = React.useState(0);
  const [albums, setAlbums] = useState([]);
  const userContext = useContext(UserContext);

  useEffect(() => {
    axios
      .get(`http://localhost:8080/artiste/${props.id}/albums`, {
        headers: { Authorization: `Bearer ${userContext.user.accessToken}` },
      })
      .then((res) => {
        setAlbums(res.data);
      });
  }, []);

  const autor = {
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elitLorem ipsum dolor sit amet, consectetur adipiscing elitLorem ipsum dolor sit amet, consectetur adipiscing elitLorem ipsum dolor sit amet, consectetur adipiscing elitLorem ipsum dolor sit amet, consectetur adipiscing elitLorem ipsum dolor sit amet, consectetur adipiscing elitLorem ipsum dolor sit amet, consectetur adipiscing elitLorem ipsum dolor sit amet, consectetur adipiscing elitLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. A erat nam at lectus urna. Scelerisque viverra mauris in aliquam sem fringilla. Lacus viverra vitae congue eu consequat ac felis. Amet est placerat in egestas erat imperdiet sed. Ante in nibh mauris cursus mattis molestie a. Nunc non blandit massa enim nec dui nunc. Tellus in hac habitasse platea. Ut sem nulla pharetra diam sit amet nisl. Quis commodo odio aenean sed adipiscing diam donec adipiscing tristique",
  };

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index) => {
    setValue(index);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static" color="transparent">
        <Tabs
          className={classes.tab}
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          variant="fullWidth"
        >
          <Tab
            style={{ color: "white" }}
            label="Présentation"
            {...a11yProps(0)}
          />
          <Tab
            style={{ color: "white" }}
            label={`Musiques (7 titres)`}
            {...a11yProps(1)}
          />
          <Tab style={{ color: "white" }} label="PLAYLISTS" {...a11yProps(2)} />
        </Tabs>
      </AppBar>
      <SwipeableViews
        axis={theme.direction === "rtl" ? "x-reverse" : "x"}
        index={value}
        onChangeIndex={handleChangeIndex}
      >
        <TabPanel value={value} index={0} dir={theme.direction}>
          <div>
            <Typography
              variant="h5"
              color=""
              component="p"
              style={{ marginBottom: "2px" }}
            >
              Biographie
            </Typography>
            <Typography variant="subtitle2">{autor.description}</Typography>
            <Typography
              variant="h5"
              color=""
              component="p"
              style={{ marginBottom: "3%", marginTop: "10%" }}
            >
              Albums
            </Typography>
            <div className={classes.container1}>
              {albums
                ? albums.map((album, index) => (
                    <div className={classes.container2}>
                      <Card className={classes.card} key={album.id}>
                        <CardMedia
                          className={classes.media}
                          image={
                            (album.image = `https://picsum.photos/600/600?random=${index}`)
                          }
                          title={album.nom}
                        />
                      </Card>
                      <div>
                        <Typography
                          className={classes.fontStyle}
                          variant="body2"
                          color="textSecondary"
                          component="p"
                        >
                          {album.nom}
                        </Typography>
                      </div>
                    </div>
                  ))
                : "Pas d'album"}
            </div>
          </div>
        </TabPanel>
        <TabPanel value={value} index={1} dir={theme.direction}>
          <BasicTable id={props.id}></BasicTable>
        </TabPanel>
        <TabPanel value={value} index={2} dir={theme.direction}>
          Les playlists de {autor.name}
        </TabPanel>
      </SwipeableViews>
    </div>
  );
}
