import React, { useState, useEffect, useContext } from "react";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import axios from "axios";
import PlaylistCard from "./PlaylistCard";
import UserContext from "../../contexte/user/UserContext";
import ModalAdd from "./ModalAdd";

export default function Playlists() {
  const [playlists, setPlaylist] = useState([]);
  const userContext = useContext(UserContext);

  useEffect(() => {
    axios
      .get(`http://localhost:8080/playlist`, {
        headers: {
          Authorization: `Bearer ${userContext.user.accessToken}`,
        },
      })
      .then((res) => {
        setPlaylist(res.data);
      });
  }, []);

  const isFavorite = (id) => {
    // this.setState({
    //   playlists: this.state.playlists.map((playlist) => {
    //     if (playlist.id === id) {
    //       playlist.isFavorite = !playlist.isFavorite;
    //     }
    //     return playlist;
    //   }),
    // });
  };

  const responsive = {
    superLargeDesktop: {
      // the naming can be any, depends on you.
      breakpoint: { max: 4000, min: 3000 },
      items: 3,
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 2,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 1,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
    },
  };
  const placementStyle = {
    marginTop: "5%",
    marginBottom: "5%",
  };
  return (
    <div style={placementStyle}>
      <h2
        style={{
          marginBottom: "2%",
          marginLeft: "3%",
          fontFamily: "open-sans",
        }}
      >
        Playlists <ModalAdd></ModalAdd>
      </h2>
      <Carousel
        swipeable={true}
        draggable={false}
        responsive={responsive}
        ssr={true} // means to render carousel on server-side.
        infinite={true}
        autoPlay="true"
        autoPlaySpeed={5000}
        keyBoardControl={true}
        customTransition="transform 500ms ease-in-out"
        centerMode={true}
        transitionDuration={500}
        containerClass="carousel-container"
        removeArrowOnDeviceType={["tablet", "mobile"]}
        itemClass="carousel-item-padding-70-px"
      >
        {playlists.map((playlist) => (
          <PlaylistCard key={playlist.id} playlist={playlist}></PlaylistCard>
        ))}
      </Carousel>
    </div>
  );
}
