import React, { useContext, useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import { Link } from "react-router-dom";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import FavoriteBorder from "@material-ui/icons/FavoriteBorder";
import Favorite from "@material-ui/icons/Favorite";
import TableRow from "@material-ui/core/TableRow";
import axios from "axios";
import UserContext from "../../contexte/user/UserContext";

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
  link: {
    textDecoration: "none",
    color: "white",
    "&:hover": {
      textDecoration: "underline",
    },
  },
});

export default function TablePlaylist(props) {
  const classes = useStyles();
  const [favoris, setFavoris] = useState([]);
  const [isFull, setIsFull] = useState(false);
  const userContext = useContext(UserContext);

  useEffect(() => {
    axios
      .get(`http://localhost:8080/user/${userContext.user.id}/favoris`, {
        headers: {
          Authorization: `Bearer ${userContext.user.accessToken}`,
        },
      })
      .then((res) => {
        setFavoris(res.data[0]);
      });
  }, []);

  const test = (event) => {
    console.log(event);
  };

  return (
    <TableContainer
      style={{ backgroundColor: "inherit", marginBottom: "10%" }}
      component="div"
    >
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell style={{ color: "lightgrey" }}>#</TableCell>
            <TableCell style={{ color: "lightgrey" }}>TITRE</TableCell>
            <TableCell style={{ color: "lightgrey" }}>ARTISTE</TableCell>
            <TableCell style={{ color: "lightgrey" }}>DUREE</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {props.titles.map((row, index) => (
            <TableRow hover key={row.id}>
              <TableCell key={index} style={{ color: "white" }}>
                <div key={index}>
                  <p key={index}>
                    {row.id}
                    {isFull ? (
                      <Favorite
                        key={row.id}
                        onClick={(e) => setIsFull(!isFull)}
                        style={{ float: "right" }}
                      />
                    ) : (
                      <FavoriteBorder
                        key={row.id}
                        onClick={(e) => setIsFull(!isFull)}
                        style={{ float: "right" }}
                      />
                    )}
                  </p>
                </div>
              </TableCell>
              <TableCell style={{ color: "white" }}>{row.nom}</TableCell>

              <TableCell>
                <Link className={classes.link} to={`/autor/${row.artiste.id}`}>
                  {row.artiste.name}
                </Link>
              </TableCell>
              <TableCell style={{ color: "white" }}>{row.duree}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
