import React, { useContext, useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import IconButton from "@material-ui/core/IconButton";
import { Link } from "react-router-dom";
import Typography from "@material-ui/core/Typography";
import FavoriteBorder from "@material-ui/icons/FavoriteBorder";
import Favorite from "@material-ui/icons/Favorite";
import CardMedia from "@material-ui/core/CardMedia";
import axios from "axios";
import UserContext from "../../contexte/user/UserContext";
import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import TablePlaylist from "./TablePlaylist";
import ModalTitreToADD from "./ModalTitreToADD";

export default function PlaylistDetails(detail) {
  const useStyles = makeStyles({
    root: {
      margin: "2%",
    },
    media: {
      height: 300,
      display: "block",
    },
    placement1: {
      height: "100%",
      width: "24%",
      display: "inline-block",
      display: "content",
      float: "left",
      margin: "0 2% 2% 0",
    },
    placement2: {
      width: "100%",
      height: "100%",
      overflow: "hidden",
      padding: "2% 0 0 2%",
      color: "white",
    },
    button: {
      backgroundColor: "#1DB954",
      color: "white",
      height: 35,
      width: 150,
      "&:hover": {
        backgroundColor: "#1DC954",
        transform: "scale(1.05)",
        boxShadow: "0px 6px 12px rgba(38, 38, 38, 0.2)",
        color: "#FFF",
      },
    },
    title: {
      fontSize: 80,
    },
  });
  const [titles, setTitles] = useState([]);
  const [playlist, setPlaylist] = useState([]);
  const [isFav, setIsFav] = useState(true);
  const userContext = useContext(UserContext);
  const { id } = detail.match.params;

  useEffect(() => {
    setIsFav(true);
    axios
      .get(`http://localhost:8080/playlist/${id}/titres`, {
        headers: { Authorization: `Bearer ${userContext.user.accessToken}` },
      })
      .then((res) => {
        setTitles(res.data);
      })
      .then(() => {
        axios
          .get(`http://localhost:8080/playlist/${id}`, {
            headers: {
              Authorization: `Bearer ${userContext.user.accessToken}`,
            },
          })
          .then((r) => {
            setPlaylist(r.data);
          });
      });
  }, []);
  console.log(playlist);
  const classes = useStyles();

  return (
    <div>
      <div className={classes.placement2}>
        <Card className={classes.placement1}>
          <CardMedia
            className={classes.media}
            image={
              playlist.image
                ? playlist.image
                : "https://picsum.photos/600/600?random=1"
            }
            title="Name"
          />
        </Card>
        <div>
          <h2 style={{ marginBottom: "4%" }}>Playlist</h2>
          <h1 className={classes.title}>{playlist.nom}</h1>
          {playlist.user ? (
            <p style={{ marginBottom: "60px", color: "#C0C0C0" }}>
              Créée par{" "}
              <strong style={{ color: "white" }}>
                {playlist.user.username}
              </strong>{" "}
              • <strong>{titles.length}</strong> titre(s)
            </p>
          ) : (
            <p style={{ marginBottom: "60px", color: "#C0C0C0" }}>
              Créée par <strong style={{ color: "white" }}>admin</strong> •{" "}
              <strong>{titles.length}</strong> titre(s)
            </p>
          )}
          <ButtonGroup disableElevation variant="contained" color="primary">
            <Button className={classes.button}>Lire</Button>
            <IconButton
              style={{
                paddingRight: "200px",
                cursor: "pointer",
                padding: 0,
                color: "whitesmoke",
              }}
              aria-label="add to favorites"
            >
              {isFav ? <Favorite /> : <FavoriteBorder />}
            </IconButton>
            <ModalTitreToADD id={id} />
          </ButtonGroup>
        </div>
      </div>
      <TablePlaylist titles={titles}></TablePlaylist>
    </div>
  );
}
