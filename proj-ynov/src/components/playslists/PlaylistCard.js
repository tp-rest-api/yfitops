import React, { Component } from "react";
import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import IconButton from "@material-ui/core/IconButton";
import { Link } from "react-router-dom";
import Typography from "@material-ui/core/Typography";
import FavoriteBorder from "@material-ui/icons/FavoriteBorder";
import Favorite from "@material-ui/icons/Favorite";

export default class PlaylistCard extends Component {
  isFavorite = () => {
    console.log();
  };
  render() {
    const root = {
      maxWidth: 300,
      marginLeft: "3%",
      marginRight: "3%",
      marginBottom: "2%",
    };
    const fontStyle = {
      display: "inline-block",
      color: "white",
      marginLeft: "4%",
      fontFamily: "Open-sans",
    };
    const media = {
      height: 150,
      paddingTop: "56.25%", // 16:9
    };

    const { id, nom, image, isFavorite } = this.props.playlist;
    return (
      <div>
        <Link style={{ textDecoration: "none" }} to={`/playlist/${id}`}>
          <Card key={id} style={root}>
            <CardMedia style={media} image={image} title={nom} />
          </Card>
        </Link>
        <div>
          <Typography
            style={fontStyle}
            variant="body2"
            color="textSecondary"
            component="p"
          >
            {nom}
          </Typography>
          <IconButton
            style={{
              float: "right",
              marginRight: "30%",
              cursor: "pointer",
              padding: 0,
              color: "whitesmoke",
            }}
            aria-label="add to favorites"
          >
            {isFavorite ? <Favorite /> : <FavoriteBorder />}
          </IconButton>
        </div>
      </div>
    );
  }
}
