import React, { useContext, useEffect, useState } from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import axios from "axios";
import Autocomplete from "@material-ui/lab/Autocomplete";
import UserContext from "../../contexte/user/UserContext";

export default function FormTitreToAdd(props) {
  const userContext = useContext(UserContext);
  const [titres, setTitres] = useState([]);

  useEffect(() => {
    axios
      .get(`http://localhost:8080/playlist/titres/notin/${props.id}`, {
        headers: { Authorization: `Bearer ${userContext.user.accessToken}` },
      })
      .then((res) => {
        setTitres(res.data);
      });
  }, []);

  const add = (event) => {
    event.preventDefault();
    const nom = event.target.test.value;
    axios
      .get(`http://localhost:8080/titre/search/${nom}`, {
        headers: { Authorization: `Bearer ${userContext.user.accessToken}` },
      })
      .then((res) => {
        const title = res.data[0];
        const body = {
          nom: title.nom,
          image: title.image,
          artiste: {
            id: title.artiste.id,
          },
          playlist: {
            id: parseInt(props.id),
          },
          duree: title.duree,
        };
        console.log(body);
        axios
          .put(`http://localhost:8080/titre/${title.id}`, body, {
            headers: {
              Authorization: `Berear eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImlhdCI6MTYwNjA2NTUyMywiZXhwIjoxNjA2MTUxOTIzfQ.d68XaRPLz0odbrzBGG6G0jsahZWBXkQaMyeoHJq2507A3azJrvVLzvMb2EgF1lUvGpo5mXUYx4umoFbvK9a4Ig`,
            },
          })
          .then((r) => {
            console.log(`${r} modified`);
          })
          .catch((e) => {
            console.log(e);
          });
      });
  };

  return (
    <div>
      <form onSubmit={add} autoComplete="off">
        <Autocomplete
          freeSolo
          id="test"
          getOptionLabel={(option) => option.nom}
          options={titres}
          renderInput={(params) => (
            <TextField
              id="titre"
              {...params}
              label="Titre ..."
              margin="normal"
              variant="outlined"
              InputProps={{ ...params.InputProps, type: "search" }}
            />
          )}
        />
        <Button type={"submit"} variant="contained" color="primary">
          Confirmer
        </Button>
      </form>
    </div>
  );
}
