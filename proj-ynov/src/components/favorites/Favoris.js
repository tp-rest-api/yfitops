import React, { useEffect, useState, useContext } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import Button from "@material-ui/core/Button";
import axios from "axios";
import UserContext from "../../contexte/user/UserContext";
import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";

export default function Favoris() {
  const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
      margin: "3%",
      color: "White",
    },
    container1: {
      width: "100%",
      height: "100%",
      overflow: "hidden",
      padding: "1% 0 0 1%",
    },
    container2: {
      marginLeft: "5%",
      height: "100%",
      width: 300,
      display: "inline-block",
      float: "left",
      margin: "0 1% 1% 0",
    },
    media: {
      height: 200,
      paddingTop: "56.25%", // 16:9
      border: "1px solid black",
    },
    card: {
      maxWidth: 400,
      marginLeft: "3%",
      marginRight: "3%",
      marginBottom: "2%",
    },
    fontStyle: {
      display: "inline-block",
      color: "white",
      marginLeft: "4%",
      fontFamily: "Open-sans",
    },
  }));
  const [favs, setFav] = useState([]);
  const userContext = useContext(UserContext);
  const classes = useStyles();

  useEffect(() => {
    axios
      .get(`http://localhost:8080/user/${userContext.user.id}/favoris`, {
        headers: {
          Authorization: `Bearer ${userContext.user.accessToken}`,
        },
      })
      .then((res) => {
        setFav(res.data[0]);
        console.log(res.data);
      });
  }, []);

  return (
    <div>
      <h2
        style={{
          marginBottom: "2%",
          marginLeft: "3%",
          fontFamily: "open-sans",
        }}
      >
        Titre(s) Favoris
      </h2>
      <div className={classes.container1}>
        {favs.titres
          ? favs.titres.map((fav, index) => (
              <div className={classes.container2}>
                <Card className={classes.card} key={fav.id}>
                  <CardMedia
                    className={classes.media}
                    image={
                      (fav.image = `https://picsum.photos/600/600?random=${index}`)
                    }
                    title={fav.nom}
                  />
                </Card>
                <div>
                  <Typography
                    className={classes.fontStyle}
                    variant="body2"
                    color="textSecondary"
                    component="p"
                  >
                    {fav.nom}
                  </Typography>
                </div>
              </div>
            ))
          : "Pas de titre"}
      </div>
    </div>
  );
}
