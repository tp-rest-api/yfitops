import React from "react";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import ArtisteCard from "./ArtisteCard";
import { Link } from "react-router-dom";

export default class AutorsCarousel extends React.Component {
  state = {
    autors: [
      {
        id: 1,
        name: "Ma chanson",
        urlImage:
          "https://cdn.pixabay.com/photo/2014/07/09/10/04/man-388104_1280.jpg",
      },
      {
        id: 2,
        name: "VIAUD Loïs",
        urlImage:
          "https://cdn.pixabay.com/photo/2015/06/19/09/39/lonely-814631_1280.jpg",
      },
      {
        id: 3,
        name: "VIAUD Loïs",
        urlImage: "https://randomuser.me/api/portraits/men/42.jpg",
      },
      {
        id: 4,
        name: "VIAUD Loïs",
        urlImage: "https://randomuser.me/api/portraits/men/43.jpg",
      },
      {
        id: 5,
        name: "VIAUD Loïs",
        urlImage: "https://randomuser.me/api/portraits/men/76.jpg",
      },
      {
        id: 6,
        name: "VIAUD Loïs",
        urlImage: "https://randomuser.me/api/portraits/women/56.jpg",
      },
      {
        id: 7,
        name: "VIAUD Loïs",
        urlImage: "https://randomuser.me/api/portraits/men/17.jpg",
      },
      {
        id: 8,
        name: "VIAUD Loïs",
        urlImage: "https://randomuser.me/api/portraits/men/45.jpg",
      },
    ],
  };

  render() {
    const responsive = {
      superLargeDesktop: {
        // the naming can be any, depends on you.
        breakpoint: { max: 4000, min: 3000 },
        items: 10,
      },
      desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 6,
      },
      tablet: {
        breakpoint: { max: 1024, min: 464 },
        items: 4,
      },
      mobile: {
        breakpoint: { max: 464, min: 0 },
        items: 2,
      },
    };
    const placementStyle = {
      marginTop: "5%",
    };
    return (
      <div style={placementStyle}>
        <h2 style={{ marginBottom: "2%", marginLeft: "3%" }}>Auteurs : </h2>
        <Carousel
          swipeable={true}
          draggable={false}
          responsive={responsive}
          ssr={true} // means to render carousel on server-side.
          infinite={true}
          autoPlay={this.props.deviceType !== "mobile" ? true : false}
          autoPlaySpeed={5000}
          keyBoardControl={true}
          customTransition="all .5 ease-in"
          centerMode={true}
          transitionDuration={500}
          containerClass="carousel-container"
          removeArrowOnDeviceType={["tablet", "mobile"]}
          deviceType={this.props.deviceType}
          itemClass="carousel-item-padding-70-px"
        >
          {this.state.autors.map((autor) => (
            <Link style={{ textDecoration: "none" }} to={`/autor/${autor.id}`}>
              <MusicCard key={autor.id} autor={autor}></MusicCard>
            </Link>
          ))}
        </Carousel>
      </div>
    );
  }
}
