import React, { useContext, useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    margin: "3%",
    color: "White",
  },
  container1: {
    width: "100%",
    height: "100%",
    overflow: "hidden",
    padding: "1% 0 0 1%",
  },
  container2: {
    marginLeft: "5%",
    height: "100%",
    width: 300,
    display: "inline-block",
    float: "left",
    margin: "0 1% 1% 0",
  },
  media: {
    height: 200,
    paddingTop: "56.25%", // 16:9
    border: "1px solid black",
  },
  card: {
    maxWidth: 400,
    marginLeft: "3%",
    marginRight: "3%",
    marginBottom: "2%",
  },
  fontStyle: {
    display: "inline-block",
    color: "white",
    marginLeft: "4%",
    fontFamily: "Open-sans",
  },
}));

export default function AutocompleteVue(props) {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <div className={classes.container1}>
        <h1 style={{ margin: "4%" }}>Titre(s)</h1>
        {props.location.state
          ? props.location.state.detail.map((titre, index) => (
              <div className={classes.container2}>
                <Card className={classes.card} key={titre.id}>
                  <CardMedia
                    className={classes.media}
                    image={
                      (titre.image = `https://picsum.photos/600/600?random=${index}`)
                    }
                    title={titre.nom}
                  />
                </Card>
                <div>
                  <Typography
                    className={classes.fontStyle}
                    variant="body2"
                    color="textSecondary"
                    component="p"
                  >
                    {titre.nom}
                  </Typography>
                </div>
              </div>
            ))
          : "Pas de titre"}
      </div>
    </div>
  );
}
