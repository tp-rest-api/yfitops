import React, { Component } from "react";
import AutorDetails from "./components/artistes/AutorDetails";
import AutorsCarousel from "./components/artistes/AutorsCarousel";
import Playlists from "./components/playslists/Playlists";
import PlaylistDetails from "./components/playslists/PlaylistDetails";
import login from "./components/SignIn/SignIn";
import search from "./components/AutocompleteVue";
import register from "./components/SignUp/SignUp";
import UserProvider from "./contexte/user/UserProvider";
import Favoris from "./components/favorites/Favoris";
import "./App.css";
import AppBar from "./components/HeaderAppBar";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

class App extends Component {
  render() {
    return (
      <UserProvider>
        <Router>
          <div className="App">
            <AppBar></AppBar>
            <Switch>
              <Route path="/" exact>
                <AutorsCarousel />
                <Playlists />
                <Favoris />
              </Route>
              <Route path="/login" exact component={login} />
              <Route path="/search" exact component={search} />
              <Route path="/register" exact component={register} />
              <Route path="/autor/:id" exact component={AutorDetails} />
              <Route path="/playlist/:id" exact component={PlaylistDetails} />
            </Switch>
          </div>
        </Router>
      </UserProvider>
    );
  }
}

export default App;
